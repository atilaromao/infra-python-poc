# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/http" {
  version = "2.1.0"
  hashes = [
    "h1:SE5ufHNXfkUaY6ZLLW742Pasb/Jx/Y/lUwoYS3XbElA=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version = "4.41.0"
  hashes = [
    "h1:P1UECzmOSVtSSenDy+GCvutFrYvuB6FlVZOAsjGrfi8=",
    "zh:03ed0ba0eeb7cf926b407c575c285ffc2c45edcb36154116a59d443b16738e92",
    "zh:074a02105ef2988c9646b21f793030a805aefcefe273822d5c41285dcaddf956",
    "zh:3a6e091fccd3e27040b36bd1ac5d0b47485174667218c6af47802b184d1d3fdc",
    "zh:3def971d5d5dbff33c1b7264654378f143d76d5de7430c187710607a0c8e0b22",
    "zh:3f343b4f87716f6269b16d7f71eeb8c66db8e6764e25071a9b288daeb1f9464d",
    "zh:52a8c88ea9c96022dfb60fe6973cb6b94aac6fc2f922dcc444bf1925dc3db7fd",
    "zh:7bd3ed0c4ffcee1dd280104ee79d7a6f2b513b8f4e4a6395f1bbf3894b41527a",
    "zh:a116e61180a033a5804d4f82e31d066f340c00029716aa3b4c0af6e098fb6826",
    "zh:b2c94b1f25c8a65e6254a9b2602996963a1a22cba6703ec7803faefc2997fcc6",
    "zh:ca562056592f610d93c8bde80f14188a82bcdf09be91a28d24c838cec5bc947e",
  ]
}
