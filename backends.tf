terraform {
  backend "http" {
    update_method = "PUT"
    address       = "https://objectstorage.sa-saopaulo-1.oraclecloud.com/p/siFVHwlSFBAUN5EYxqJ-TiJlg5FXIzZSpm1kSmLz8TdQ6JiQQi64nb9v6MeSpoTl/n/grxrucnkeoss/b/sonar-tfstate/o/terraform.tfstate"
  }
}