resource "oci_core_instance" "instancia_atila" {
  availability_domain = var.ad
  compartment_id      = var.compartment_id
  shape               = var.instance_shape
  create_vnic_details {
    assign_public_ip = true
    subnet_id        = oci_core_subnet.sonar_subnet.id
    nsg_ids = [
      oci_core_network_security_group.sonar_network_security_group.id,
    ]
  }
  display_name = "sonar-qube-instance"
  source_details {
    source_id   = var.image_id
    source_type = "image"
  }
  metadata = {
    ssh_authorized_keys = var.ssh_key
    user_data           = base64encode(file(var.bootstrap_file_name))
  }
  depends_on = [
    oci_core_network_security_group.sonar_network_security_group,
    oci_core_subnet.sonar_subnet
  ]

}