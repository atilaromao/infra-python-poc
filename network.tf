resource "oci_core_vcn" "sonar_vcn" {
  #Required
  compartment_id = var.compartment_id

  #Optional
  cidr_blocks  = var.vcn_cidr_blocks
  display_name = var.vcn_display_name
  dns_label    = var.vcn_dns_label
}

resource "oci_core_subnet" "sonar_subnet" {
  #Required
  cidr_block     = var.subnet_cidr_block
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.sonar_vcn.id

  #Optional
  route_table_id = oci_core_route_table.sonar_route_table.id
  depends_on = [
    oci_core_vcn.sonar_vcn,
    oci_core_route_table.sonar_route_table
  ]
}

resource "oci_core_route_table" "sonar_route_table" {
  #Required
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.sonar_vcn.id

  #Optional
  route_rules {
    #Required
    network_entity_id = oci_core_internet_gateway.sonar_internet_gateway.id

    #Optional
    destination      = var.route_table_route_rules_destination
    destination_type = var.route_table_route_rules_destination_type
  }
  depends_on = [
    oci_core_vcn.sonar_vcn,
    oci_core_internet_gateway.sonar_internet_gateway
  ]
}

resource "oci_core_internet_gateway" "sonar_internet_gateway" {
  #Required
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.sonar_vcn.id

  #Optional
  enabled = var.internet_gateway_enabled
  depends_on = [
    oci_core_vcn.sonar_vcn,
  ]
}

resource "oci_core_network_security_group" "sonar_network_security_group" {
  #Required
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.sonar_vcn.id

  #Optional
  display_name = var.network_security_group_display_name
}

resource "oci_core_network_security_group_security_rule" "sonar_network_security_group_security_rule" {
  #Required
  network_security_group_id = oci_core_network_security_group.sonar_network_security_group.id
  direction                 = var.network_security_group_security_rule_direction
  protocol                  = var.network_security_group_security_rule_protocol

  #Optional
  description      = var.network_security_group_security_rule_description
  source           = var.network_security_group_security_rule_source
  source_type      = var.network_security_group_security_rule_source_type
  depends_on = [
    oci_core_network_security_group.sonar_network_security_group,
  ]
}