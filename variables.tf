#General
variable "region" {
  type        = string
  default     = "sa-saopaulo-1"
  description = "Brazil region for OCI"
}

variable "key_path" {
  type        = string
  default     = "./atila.romao-08-30-20-54.pem"
  description = "description"
}

variable "user_ocid" {
  type        = string
  default     = "ocid1.user.oc1..aaaaaaaadp6mvtlvcnfdtgengsqy2m7uno26zcarkhrwmk6a6i7q46itrgzq"
  description = "User ocid"
}

variable "tenancy_ocid" {
  type        = string
  default     = "ocid1.tenancy.oc1..aaaaaaaa45petfifmx6ghlwkuhxvcnjjk7zbitul3rribdxvzxbujseqqrwa"
  description = "Tenancy ocid"
}

variable "fingerprint" {
  type        = string
  default     = "42:c0:b3:95:41:39:37:50:3b:0d:08:f6:bd:b1:26:4f"
  description = "fingerprint value"
}


variable "compartment_id" {
  type        = string
  default     = "ocid1.compartment.oc1..aaaaaaaaysr7v5hkx37k6mpjl6fesjpsaf4633244bqgrgskwkah2nlp3r2a"
  description = "description"
}


variable "subnet_id" {
  type        = string
  default     = "ocid1.subnet.oc1.sa-saopaulo-1.aaaaaaaaoldozvfkbldzb4o43pybfshzb6toery4tycflw5fzb3h37q6zd5q"
  description = "subnet_id"

}

variable "vcn_id" {
  type        = string
  default     = "ocid1.vcn.oc1.sa-saopaulo-1.amaaaaaa4vgrtwaay3s7sdhm6pwdzacb7ti75umzqxn6srbgqkg2pwujyrda"
  description = "description"
}

#Instance variables
variable "ad" {
  type        = string
  default     = "fVkV:SA-SAOPAULO-1-AD-1"
  description = "Brazil region for OCI"
}

variable "ssh_key" {
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDZRnsgHKg0I84XT8aMjXn3lSXTsAL83WvcCrXHDNzLr+g2o1gsMDaHQrqcifBzcjxEOwG9HdYzOfed7+unZdzBDX4wnypuRFJ948iUiZDlQk6do/2i/tS3+G1l1T/un2hHZ9Jap+Clsfvg9srFtSX5xg8UGF5r2bXqg9GrO9+//EOPUK+yDXW5vl/kXGUAcSI9SfkvPRo6Eu6KJB1hPC2Oi260kchix4N4zZ6XcZ14Wu3TG+BlJ/InqAPSvVY6+WX+uuktjZF0kqceugO2qBA07FWzrNUbPvLmKbEW60KBnQtT3GhWrXH8FBM43D8VBBe9nNV7mfVdkEmRJEUQ9J2QjwOA1qLiw0mzZVuzVSMvuH82fxWzaevQr0Do6f7wuFIq2lwCEWlHJvTuqGkk3sHpKTR2atfn0UlxdyTPid0biXpxHeJebuzZQM5Drl3EXsdGsNnKvWdqCdUKgWDIqYPQDFhkIZxTbTBN1JWw6oos7YE29FPHMykGLjP+o0WBuM= atila@DESKTOP-MSTVP0C"
  description = "public key value"
}

variable "instance_shape" {
  type        = string
  default     = "VM.Standard.E2.1.Micro"
  description = "description"
}

variable "image_id" {
  type        = string
  default     = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaocax5uxxfuum6rgpnlymsustzdcdejilsrnxkc2cgt4mmg3fbsnq"
  description = "image ocid"
}

variable "bootstrap_file_name" {
  type        = string
  default     = "bootstrap"
  description = "User data script to be executed when instance is created"
}

#Network variables

#VCN
variable "vcn_cidr_blocks" {
  type        = list(any)
  default     = ["10.0.0.0/16"]
  description = "cidr block"
}

variable "vcn_display_name" {
  type        = string
  default     = "sonar_vcn"
  description = "vcn name"
}

variable "vcn_dns_label" {
  type        = string
  default     = "sonarvcnlabel"
  description = "description"
}

#SUBNET
variable "subnet_cidr_block" {
  type        = string
  default     = "10.0.1.0/24"
  description = "description"
}


#ROUTE_TABEL
variable "route_table_route_rules_destination_type" {
  type        = string
  default     = "CIDR_BLOCK"
  description = "description"
}

variable "route_table_route_rules_destination" {
  type        = string
  default     = "0.0.0.0/0"
  description = "Allowing acces from all IPs"
}

#INTERNET_GATEWAY
variable "internet_gateway_enabled" {
  type        = string
  default     = true
  description = "description"
}

#NETWORK_SG

variable "network_security_group_display_name" {
  type        = string
  default     = "sonar_network_security_group"
  description = "description"
}

variable "network_security_group_security_rule_direction" {
  type        = string
  default     = "INGRESS"
  description = "description"
}

variable "network_security_group_security_rule_protocol" {
  type        = string
  default     = "all"
  description = "allow acces from all protocols"
}

variable "network_security_group_security_rule_source_type" {
  type        = string
  default     = "CIDR_BLOCK"
  description = "description"
}

variable "network_security_group_security_rule_source" {
  type        = string
  default     = "0.0.0.0/0"
  description = "description"
}

variable network_security_group_security_rule_description {
  type        = string
  default     = "sonar-security-group"
  description = "description"
}
